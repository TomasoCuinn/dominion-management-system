<?php
	require 'header.php';
	include_once './classes/classDatacard.php';
?>
	<div class="item3">
		<h3>Datacard Overview</h3>
		<p>Update Datacards
		<button onclick="window.location.href='https://swc-bsd.com/datacards.php?&update=bseng';">ENG</button>
		<button onclick="window.location.href='https://swc-bsd.com/datacards.php?&update=bsext';">EXT</button>
		<p>

		<p>Show Datacards
		<!--Show all Dominion datacard list-->
		<button onclick="window.location.href='https://swc-bsd.com/datacards.php?&list=bsd';">All DCs</button>
		<!--Show BSEngineering datacard list-->
		<button onclick="window.location.href='https://swc-bsd.com/datacards.php?&list=bseng';">Engineering DCs</button>
		<!--Show BSExtractions datacard list-->
		<button onclick="window.location.href='https://swc-bsd.com/datacards.php?&list=bsext';">Extractions DCs</button>
		</p>
		<p>

		<form> <!--Form for searching datacards assignments-->
			<label for="search">Search DCs: </label>
			<input type="text" id="search" name="search" minlength="3">
			<input type="submit" value="Submit">
		</form>
		</p>

		<h4>Assign Datacards</h4>
		<form> <!--Form for assigning datacards-->
			<p><label for="datacard">Datacard:</label>
			<select name="datacard" id="datacard">
				<?php Datacard::datacardArraySelect();?>
			</select>

			<label for="id">Entity ID#: </label>
			<input type="text" id="id" name="id" value="<?php if(isset($_GET['id'])) {echo $_GET['id'];}; ?>">

			<label for="entity_type">Entity Type: </label>
			<select name="entity_type" id="entity_type">
				<option value="factory">Factory</option>
				<option value="shipyard">Shipyard</option>
			</select>

			<label for="uses">Uses: </label>
			<input type="number" min="-1" max="1000" id="uses" name="uses" value="<?php if(isset($_GET['uses'])) {echo $_GET['uses'];}; ?>">

			<input type="submit" value="submit">
		</form></p>
	
		<p>
		<?php
		if(isset($_GET)) {
			$db = new Database(); //Establish connection to database

			if(isset($_GET['datacard'])) {
				Datacard::assignDatacard($db);
			} elseif(isset($_GET['update'])) {
				Datacard::updateDatacards($db); //Pull new datacards from API
			} elseif(isset($_GET['list'])) {
				Datacard::pullDatacardsByFaction($db, $_GET['list']); //List datacards for specific faction
			} elseif(isset($_GET['search'])) {
				Datacard::pullDatacardsBySearch($db, $_GET['search']); //Perform user search for datacards
			}
			$db->disconnect(); //End connection to database
		}
		?>
		</p>
	</div>
<?php
	require 'footer.php';
?>
