<?php
	require 'header.php';
	include_once 'classes/classSession.php';
	include_once 'classes/classTokens.php';
?>
	<div class="item3">
		<p><button onclick="window.location.href='https://swc-bsd.com/tokens.php?&update=yes';">Update Tokens</button></p>
		<?php
		if(isset($_GET['update'])) {
			Tokens::updateAllTokens(); //Updates access/refresh tokens for all accounts
		}
		?>
	</div>
<?php
	require 'footer.php';
?>
