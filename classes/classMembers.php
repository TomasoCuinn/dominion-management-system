<?php
	include_once './includes/dbh.inc.php';
	include_once './includes/datecalc.inc.php';
	include_once 'classTokens.php';

//Handle member functions
class Members {
	//Gets memberlist from specific faction and inserts members into database table
	static function updateFactionMembers(&$db) {
		if(isset($_GET['update'])) {
			//Determine which faction is used
			if($_GET['update'] == "bsenf") {
				$handle = "Victor O`Cuinn"; //Set handle as leader of faction
				$faction = "Blue Star Enforcement"; //Set faction
			} elseif($_GET['update'] == "bseng") {
				$handle = "Tomas O`Cuinn"; //Set handle as leader of faction
				$faction = "Blue Star Engineering"; //Set faction
			} elseif($_GET['update'] == "bsext") {
				$handle = "Pursh Greene"; //Set handle as leader of faction
				$faction = "Blue Star Extractions"; //Set faction
			};

			$updateToken = Tokens::updateAccessToken($db, $handle); //Refresh access/refresh tokens
			if(!$updateToken) {return;} //Do not proceed if access token is not refreshed
			$accessToken = Tokens::dbAccessToken($db, $handle); //Get new access token from db
			self::insertMembers($db, $faction, $accessToken); //Insert memberlist into db
			} else {header('Location: https://swc-bsd.com/members.php');} //If GET update not set, reload members
			header('Location: https://swc-bsd.com/members.php'); //Reload members upon success
		}

	//Inserts memberlist from API into database table
	static function insertMembers(&$db, &$faction, &$accessToken) {
		//Use access token to obtain faction memberlist
		$url = 'https://www.swcombine.com/ws/v2.0/faction/'.$faction.'/members/?access_token='.$accessToken;

		$ch = curl_init(); //Initiate cURL session
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //Return page contents as string
		curl_setopt($ch, CURLOPT_URL, $url); //URL to fetch
		$result = curl_exec($ch); //Execute cURL session
		$xml = simplexml_load_string($result); //Convert string into object

		$db->connection->begin_transaction(); //Begin SQL DB transaction
		$stmt = $db->connection->prepare("INSERT INTO `members` (`handle`, `rank`, `faction`, `joinDate`, `lastLogin`, `active`) VALUES (?, ?, ?, ?, ?, ?) ON DUPLICATE KEY UPDATE `faction` = ?, `lastLogin` = ?, `active` = ?"); //Build query

		//Insert faction members into database
		foreach($xml->members->member as $member) {
			$handle = (string) ($member->name); //Character handle
			$rank = (string) ($member->infofields->infofield); //Infofield 1
			$lastLogin = (string) ($member->lastlogin->timestamp); //Last login date
			$active = (string) ($member->active); //Whether SWC considers character active
			$joinDate = "0"; //Join date will be set manually
			$stmt->bind_param("sssssssss", $handle, $rank, $faction, $joinDate, $lastLogin, $active, $faction, $lastLogin, $active); //Bind arameters
			$stmt->execute(); //Execute statement
		}
		$db->connection->commit(); //Commit to database
	}

	//Get members from database table and display them
	static function displayMembers(&$db) {
		$sql = "SELECT * FROM `members` WHERE `faction` LIKE 'Blue Star%'"; //Build query
		$result = mysqli_query($db->connection, $sql); //Perform query
		$resultCheck = mysqli_num_rows($result); //Return # of rows from query

		if ($resultCheck > 0) {
			//If rows exist
			if(isset($_GET['copypaste'])) {
				//If _GET copypaste is set, display a credit bulk event for c/p ?>
				<!--Button to replace copy/paste list with table-->
				<button onclick="window.location.href='https://swc-bsd.com/members.php';">Convert Copy/Paste to Table</button>
				</p><p>
				<?php
				$sql = "SELECT * FROM `members` WHERE `faction` LIKE 'Blue Star%' AND `active` = 'true'"; //Build query
				$result = mysqli_query($db->connection, $sql); //Perform query

				while($row = mysqli_fetch_assoc($result)) {
					$handle = $row['handle']; //Character handle
					$rank = $row['rank']; //Infofield 1
					$oneMonth = 31536000 / 12; //One month in unix time
					$seniorityMonth = floor((time() - $row['joinDate']) / $oneMonth); //Character seniority with
					$salaryBase = "2000000" + ("100000" * $seniorityMonth); //Base weekly salary
					$salaryCap = "3500000"; //Base salary cap

					self::modifySalaryByRank($rank);

					$salary = min($salaryBase, $salaryCap); //Determine actual salary

					echo "". $handle ."|". $salary ."|Dominion Salary (". $seniorityMonth ." months seniority)<br />"; //Build bulk credit event for each member
				}
			} else {
				//If _GET copypaste is not set, display regular memberlist table ?>
				<!--Button to replace table with copy/paste list-->
				<button onclick="window.location.href='https://swc-bsd.com/members.php?&copypaste=yes';">Convert Table to Copy/Paste</button>
				</p>
				<p>
				<table>
					<tr>
						<th>Handle</th>
						<th>Rank</th>
						<th>Faction</th>
						<th>Join Date</th>
						<th>Seniority</th>
						<th>Salary</th>
						<th>Login</th>
						<th>Active</th>
					</tr>
				<?php
				while($row = mysqli_fetch_assoc($result)) {
					$handle = $row['handle']; //Character handle
					$rank = $row['rank']; //Infofield 1
					$oneMonth = 31536000 / 12; //One month in unix time
					$seniorityMonth = floor((time() - $row['joinDate']) / $oneMonth); //Character seniority with
					$salaryBase = "2000000" + ("100000" * $seniorityMonth); //Base weekly salary
					$salaryCap = "3500000"; //Base salary cap
					$faction = str_replace(['Blue Star Engineering', 'Blue Star Extractions', 'Blue Star Enforcement'], ['Eng', 'Ext', 'Enf'], $row['faction']); //Abbreviate character's faction
					$joinDate = formatDateYD($row['joinDate']); //Convert join date from unix to YXX DXXX
					$lastLogin = formatDateYD($row['lastLogin']);
					$active = $row['active'];

					if($row['joinDate'] == "0"){
						$joinDate = "-"; //If join date is empty, set join date to "-"
						$seniority = "-"; //If join date is empty, set seniority to "-"
					};
					if($row['lastLogin'] == "0"){
						$lastLogin = "-"; //If join date is empty, set to "-" to prevent display error
					};

					self::modifySalaryByRank($rank);

					$salary = min($salaryBase, $salaryCap); //Determine actual salary

					//Build memberlist table
					echo "<tr>";
						echo "<td>". $handle ."</td>";
						echo "<td>". $rank ."</td>";
						echo "<td>". $faction ."</td>";
						echo "<td>". $joinDate ."</td>";
						echo "<td>". $seniorityMonth ." months</td>";
						echo "<td>". numForm($salary, 0) ."</td>";
						echo "<td>". $lastLogin;
						echo "<td>". $active ."</td>";
					echo "</tr>";
				}
				echo "</table>";
			}
		} else {
			//If table is empty
			echo "Failed to pull member list.";
		}
	}

	//Modify salary and salary cap by rank
	static function modifySalaryByRank(&$rank) {
		if($rank == "Warden") {
			$salaryCap = "4250000"; //Salary cap for rank Warden
		}
		elseif($rank == "Sentinel") {
			$salaryCap = "5000000"; //Salary cap for rank Sentinel
		}
		elseif($rank == "Arbiter") {
			$salaryCap = "7000000"; //Salary cap for rank Arbiter
			$salaryBase = "7000000"; //Base salary for rank Arbiter
		} elseif($rank == "Marshal") {
			$salaryCap = "10000000"; //Salary cap for rank Marshal
			$salaryBase = "10000000"; //Base salary for rank Marshal
		}
	}
}
?>
