<?php
	include_once './includes/dbh.inc.php';
	include_once 'classTokens.php';

//Handle member functions
class Inventory {
	//Sanitize user input, just in case
	static function sanitizeInput(&$input) {
		$data1 = str_replace(array('/', '\'', '"', ';', '<', '>', '&'), '', $input);
		$data2 = filter_var($data1, FILTER_SANITIZE_STRING);
		return $data2;
	}

	//Send inventory action to SWC API
	static function submitAction() {
		$faction = self::sanitizeInput($_GET['faction']); //Sanitize user input
		$entityType = self::sanitizeInput($_GET['entity_type']); //Sanitize user input
		$id = self::sanitizeInput($_GET['id']); //Sanitize user input
		$property = self::sanitizeInput($_GET['property']); //Sanitize user input
		$value = self::sanitizeInput($_GET['value']); //Sanitize user input

		//Establish connection to database
		$db = new Database();
		$db->connect();

		//Refresh access tokens depending on selected faction
		if($faction == "Blue Star Enforcement") {
			Tokens::updateAccessToken($db, "Victor O`Cuinn"); //Grab new tokens
			$accessToken = Tokens::dbAccessToken($db, "Victor O`Cuinn"); //Get refreshed access token
		} elseif($faction == "Blue Star Engineering") {
			Tokens::updateAccessToken($db, "Tomas O`Cuinn"); //Grab new tokens
			$accessToken = Tokens::dbAccessToken($db, "Tomas O`Cuinn"); //Get refreshed access token
		} elseif($faction == "Blue Star Extractions") {
			Tokens::updateAccessToken($db, "Pursh Greene"); //Grab new tokens
			$accessToken = Tokens::dbAccessToken($db, "Pursh Greene"); //Get refreshed access token
		} else {
			echo "Incorrect faction selection."; //Faction GET was not set
			return;
		}

		//End connection to database
		$db->disconnect();
		unset($db);

		//Select correct entity URL UID based on entity type
		if($entityType == "ships") {
			$typeUID = "2";
		} elseif($entityType == "stations") {
			$typeUID = "5";
		} elseif($entityType == "facilities") {
			$typeUID = "4";
		} else {
			echo "Incorrect entity type selection."; //entity_type GET was not ships, stations, or facilities
			return;
		}

		$url = 'https://www.swcombine.com/ws/v2.0/inventory/'. $entityType .'/'. $typeUID .':'. $id .'/'. $property .'/?access_token='. $accessToken;

		//https://www.swcombine.com/ws/v2.0/inventory/ {entity_type:} / {uid:\d+:\d+} / {property:} /

		$postdata = http_build_query(array('new_value' => $value));
	
		$ch = curl_init(); //Initiate cURL session
		curl_setopt($ch, CURLOPT_URL, $url); //URL to fetch
		curl_setopt($ch, CURLOPT_HEADER, 0); //Include header in output
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata); //Data array to send in POST
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //Return page contents as string
		
		$result = curl_exec($ch); //Execute cURL session
		$result = simplexml_load_string($result); //Convert string into object
		$result = json_encode($result); //Encode array into JSON object
		$result = json_decode($result); //Decode JSON object into PHP object
		curl_close($ch); //Close cURL session
		echo "Success!";
	}

	//Load and display inventory through API
	static function loadInventory() {
		$load = self::sanitizeInput($_GET['load']); //Sanitize user input
		$type = self::sanitizeInput($_GET['type']); //Sanitize user input

		if($load == "bsenf") {
			$faction = "Blue Star Enforcement";
			$leader = "Victor O`Cuinn";
		} elseif($load == "bseng") {
			$faction = "Blue Star Engineering";
			$leader = "Tomas O`Cuinn";
		} elseif($load == "bsext") {
			$faction = "Blue Star Extractions";
			$leader = "Pursh Greene";
		} else {
			echo "<p>Incorrect faction selection.</p>"; //Faction GET was not correct
			return;
		}

		//Establish connection to database
		$db = new Database();
		$db->connect();

		Tokens::updateAccessToken($db, $leader); //Grab new tokens
		$accessToken = Tokens::dbAccessToken($db, $leader); //Get refreshed access token

		//End connection to database
		$db->disconnect();
		unset($db);

		/* Commented out due for future work with arrays
		$filter1 = http_build_query(array("filter_type" => "type", "filter_incusion" => "includes")); 
		$filter2 = http_build_query(array("filter_value" => "4:4"));
		*/

		$url = 'https://www.swcombine.com/ws/v2.0/inventory/'. $faction .'/facilities/owner/?&access_token='. $accessToken; //Build URL

		$ch = curl_init(); //Initiate cURL session
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //Return page contents as string
		curl_setopt($ch, CURLOPT_URL, $url); //Set built URL
		$result = curl_exec($ch); //Execute cURL session
		$xml = simplexml_load_string($result); //Convert string into object
		$httpRet = curl_getinfo($ch, CURLINFO_HTTP_CODE); //Get return code and potential error message

		if($httpRet == 200){
			?>z
			<table>
				<tr>
					<th style="text-align: left">Image</th>
					<th style="text-align: left">Type<br />Name<br />ID#</th>
					<th style="text-align: left">Location</th>
					<th style="text-align: left">Manager<br />Operator</th>
				</tr>
			<?php
			foreach($xml->entities->entity as $facility) {
				$facilImage = $facility->images->small; //Entity image
				$facilType = $facility->type; //Entity type
				$facilUID = str_replace(array("4:", "5:"), '', $facility->uid); //Entity ID
				$facilName = $facility->name; //Entity infotext
				$facilSector = $facility->location->sector; //Sector entity is inside
				$facilSystem = $facility->location->system;//System entity is inside
				$facilPlanet = $facility->location->planet;//Planet entity is inside
				$facilCity = $facility->location->city; //City entity is inside
				$facilManager = $facility->commander; //Entity manager assignment
				if($facilManager == "") {$facilManager = "None";} //If no manager is assigned, display none
				$facilOperator = $facility->pilot; //Entity operator assignment
				if($facilOperator == "") {$facilOperator = "None";} //If no operator is assigned, display none

				echo "<tr>";
					echo '<td><img src="'. $facilImage .'" style="max-height: 100px; max-width: 100px; height: auto; width: auto"></td>';
					echo '<td style="text-align: left">'. $facilType .'<br />'. $facilName .'</a><br />'. $facilUID;
					echo '<td style="text-align: left">'. $facilSector .'<br />'. $facilSystem .'<br />'. $facilPlanet .'<br />'. $facilCity .'</td>';
					echo '<td style="text-align: left">'. $facilManager .'<br />'. $facilOperator .'</td>';
				echo "</tr>";
			}
			?>
			</table>
			<?php	
		} elseif($httpRet == 400){
			echo "Request failed, error code: ". $httpRet ." Bad Request: One or more of the provided parameters was invalid.";
		} elseif($httpRet == 401){
			echo "Request failed, error code: ". $httpRet ." Unauthorized: The client does not have the required permission to access this resource.";
		} elseif($httpRet == 404){
			echo "Request failed, error code: ". $httpRet ." Not Found: The specified character does not exist or you do not have authorisation to access this inventory.";
		} else {
			echo "Request failed due to unknown error!";
		}
	}

}
?>
