<?php
include_once './includes/dbh.inc.php';
include_once './includes/functions.inc.php';
include_once 'classTime.php';
include_once 'classInventory.php';

class Datacard {
	//Searches datacards by faction
	static function pullDatacardsByFaction(&$db, &$faction) {
		//Sanitize user input
		$faction = str_replace(array('\'', '"', ',', ';', '<', '>', '&'), '', $faction);
		$faction = filter_var($faction, FILTER_SANITIZE_STRING);
		$faction = mysqli_real_escape_string($db->connection, $faction);

		if($faction == "bsd") {
			$faction = "%Blue Star%";
		} elseif($faction == "bseng") {
			$faction = "Blue Star Engineering";
		} elseif($faction == "bsext") {
			$faction = "Blue Star Extractions";
		} else {}
		$sql = $db->connection->prepare("SELECT * FROM `datacards` WHERE `faction` LIKE ?");
		$sql->bind_param("s", $faction);
		$sql->execute();
		$result = $sql->get_result();
		$fetchResult = $result->fetch_assoc();
		$resultCheck = $result->num_rows;
			
		self::displayDatacards($result, $resultCheck);
	}

	//Searches datacards by user search
	static function pullDatacardsBySearch(&$db, &$search) {
		//Sanitize user input
		$search = str_replace(array('\'', '"', ',', ';', '<', '>', '&'), '', $search);
		$search = filter_var($search, FILTER_SANITIZE_STRING);
		$search = mysqli_real_escape_string($db->connection, $search);
		$search = "%". $search ."%";

		$sql = $db->connection->prepare("SELECT * FROM `datacards` WHERE `datacard` LIKE ? OR `entityID` LIKE ? OR `entityType` LIKE ? OR `entityName` LIKE ? OR `entityOwner` LIKE ? OR `assignedQty` LIKE ?");
		$sql->bind_param("ssssss", $search, $search, $search, $search, $search, $search);
		$sql->execute();
		$result = $sql->get_result();
		$resultCheck = $result->num_rows;

		self::displayDatacards($result, $resultCheck);
	}

	//Displays datacards from prior pullDatacardsByX functions
	static function displayDatacards(&$result, &$resultCheck) {
		if($resultCheck > 0) {
			//If rows exist
			echo "<p><span style='color: gold'>Your search generated <span style='font-weight: bold'>". number_format($resultCheck) ."</span> records.</span></p>";
			?>
			<table>
				<tr>
					<th>Datacard</th>
					<th>Entity</th>
					<th>ID</th>
					<th>Name</th>
					<th>Owner</th>
					<th>Assigns</th>
				</tr>
			<?php
			while($row = $result->fetch_assoc()) {
				//Build HTML table from database table
				echo "<tr>";
					echo "<td>". $row['datacard'] ."</td>";
					echo "<td>". $row['entityType'] ."</td>";
					echo "<td>". $row['entityID'] ."</td>";
					echo "<td>". $row['entityName'] ."</td>";
					echo "<td>". $row['entityOwner'] ."</td>";
					echo "<td>". str_replace('Unl', "∞", $row['assignedQty']) ."</td>";
				echo "</tr>";
			} ?>
			</table>
		<?php
		} elseif(($resultCheck == 0)) {
			//If no rows exist
			echo "<p><span style='color: gold'>Your search generated <span style='font-weight: bold'>". number_format($resultCheck) ."</span> records.</span></p>";
		} else {
			//If query and table fail
			echo "<p><span style='color: gold'>Failed to pull datacard list.</span></p>";
		}
	}

	//Pull datacard assignments from API and insert into database
	static function loadDatacard(&$db, &$dc, $faction, &$accessToken) {
		$url = 'https://www.swcombine.com/ws/v2.0/datacard/'.$dc.'/?access_token='.$accessToken;

		$ch = curl_init(); //Initiate cURL session
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //Return page contents as string
		curl_setopt($ch, CURLOPT_URL, $url); //URL to fetch
		$result = curl_exec($ch); //Execute cURL session
		$xml = simplexml_load_string($result); //Convert string into object

		//Build SQL query to insert new data
		$db->connection->begin_transaction();
		$stmt = $db->connection->prepare("INSERT INTO `datacards` (`faction`, `datacard`, `entityID`, `entityType`, `entityName`, `entityOwner`, `assignedQty`) VALUES (?, ?, ?, ?, ?, ?, ?)");

		//Pull datacard name
		$datacardName = (string) $xml->datacard->entity;

		//Insert data into datacards table
		foreach($xml->datacard->assignedlocations->assignedlocation as $factory) {
			$entityName = (string) ($factory->location);
			$entityID = (string) str_replace(array('4:', '5:'), '', $factory->location['uid'], ); //Remove "4:" & "5:" from ID
			$entityOwner = (string) ($factory->locationowner);
			$entityType = (string) ($factory->locationtype);
			$assignedQty = (string) str_replace("-1", "Unl", $factory->quantity);
			$stmt->bind_param("sssssss", $faction, $datacardName, $entityID, $entityType, $entityName, $entityOwner, $assignedQty);
			$stmt->execute();
		}
		$db->connection->commit();
	}

	//Delete existing datacard records and insert new rows from API
	static function updateDatacards(&$db) {
		if($_GET['update'] == "bseng") {
			$updateToken = Tokens::updateAccessToken($db, "Tomas O`Cuinn"); //Grab new tokens
			$accessToken = Tokens::dbAccessToken($db, "Tomas O`Cuinn"); //Get refreshed access token

			if(!$updateToken) {return;} //Do not proceed if access token is not refreshed

			if(isset($_SESSION['privDC'])) {
				//Check if privDC SESSION variable is set from oauth
				$db->connection->begin_transaction();
				$stmtDeleteDC = $db->connection->prepare("DELETE FROM `datacards` WHERE `faction` = 'Blue Star Engineering'");
				$stmtDeleteDC->execute();
				$db->connection->commit();

				//Establish datacard UID if faction = Blue Star Engineering
				$dcEng = array("BR-23 Courier" => "14:12358921", "GAT-12h Skipray Blastboat" => "14:12358706", "Holoprojector" => "14:12359543", "Jetpack" => "14:12359042", "Keycard" => "14:12358655", "KRS-1 Ore Hauler" => "14:12358722", "Raider-class Missile Corvette" => "14:12358796", "Sub-Space Radio" => "14:12359544", "T-Series Tactical Droid" => "14:12359074",	"Vulture-class Droid Starfighter" => "14:12358808");

				//Loop through array to pull all DC assigns and insert into datacards table
				foreach($dcEng as $key => $val) {
					self::loadDatacard($db, $val, "Blue Star Engineering", $accessToken);
				}
			}
		} elseif($_GET['update'] == "bsext") {
			$updateToken = Tokens::updateAccessToken($db, "Pursh Greene"); //Grab new tokens
			$accessToken = Tokens::dbAccessToken($db, "Pursh Greene"); //Get refreshed access token

			if(!$updateToken) {return;} //Do not proceed if access token is not refreshed

			//Check if privDC SESSION variable is set from oauth
			if(isset($_SESSION['privDC'])) {
				$db->connection->begin_transaction();
				$stmtDeleteDC = $db->connection->prepare("DELETE FROM `datacards` WHERE `faction` = 'Blue Star Extractions'");
				$stmtDeleteDC->execute();
				$db->connection->commit();

				//Build array using datacard UIDs for Blue Star Extractions
				$dcExt = array("Y-8 Mining Vessel" => "14:12337107", "C-wing Ugly" => "14:12358155", "GRZ-6B Wrecker" => "14:12358154", "TIE-wing Ugly" => "14:12358153", "X-ceptor Ugly" => "14:12358152", "Y-TIE Ugly" => "14:12358151", "MC-30b Corvette" => "14:12358868", "FK-7 Airspeeder" => "14:12337109", "SX-65 Groundhog" => "14:12337108", "EVS Construction Droid" => "14:12358156", "Alpha Plus Charge" => "14:12337112", "Blaststick" => "14:12337111", "LIN-series Miner" => "14:12337110");

				//Loop through array to pull all DC assigns and insert into datacards table
				foreach($dcExt as $key => $val) {
					self::loadDatacard($db, $val, "Blue Star Extractions", $accessToken);
				}
			}
		}
	}


	//Display datacard array as form select options (dropdown)
	static function datacardArraySelect() {
		$datacardArray = array("Alpha Plus Charge" => "14:12337112", "Blaststick" => "14:12337111", "C-wing Ugly" => "14:12358155", "EVS Construction Droid" => "14:12358156", "FK-7 Airspeeder" => "14:12337109", "GRZ-6B Wrecker" => "14:12358154", "LIN-series Miner" => "14:12337110", "MC-30b Corvette" => "14:12358868", "SX-65 Groundhog" => "14:12337108", "TIE-wing Ugly" => "14:12358153", "X-ceptor Ugly" => "14:12358152", "Y-8 Mining Vessel" => "14:12337107", "Y-TIE Ugly" => "14:12358151", "BR-23 Courier" => "14:12358921", "GAT-12h Skipray Blastboat" => "14:12358706", "Holoprojector" => "14:12359543", "Jetpack" => "14:12359042", "Keycard" => "14:12358655", "KRS-1 Ore Hauler" => "14:12358722", "Raider-class Missile Corvette" => "14:12358796", "Sub-Space Radio" => "14:12359544", "T-Series Tactical Droid" => "14:12359074", "Vulture-class Droid Starfighter" => "14:12358808");

		foreach($datacardArray as $name => $uid) {
			echo '"<option value="' . $uid . '">' . $name . '</option>';
		  }
	}

	//Assign datacard through SWC API
	static function assignDatacard(&$db) {
		$datacard = Inventory::sanitizeInput($_GET['datacard']); //Sanitize user input
		$entityType = Inventory::sanitizeInput($_GET['entity_type']); //Sanitize user input
		$id = explode(",", Inventory::sanitizeInput($_GET['id'])); //Sanitize user input & convert string into array
		$uses = Inventory::sanitizeInput($_GET['uses']); //Sanitize user input

		//Verify that entity_type GET is correctly set
		if($entityType == "factory") {
			$typeUID = "4:"; //Modify $id by adding 4: to identify facility
		} elseif($entityType == "shipyard") {
			$typeUID = "5:"; //Modify $id by adding 5: to identify station
		} else {
			echo "Incorrect entity type selection."; //GET was not factory or shipyard
			return;
		}

		foreach($id as $key => $value) {$id[$key] = $typeUID ."". $value;} //Append production entity type UID before each datacard ID

		//Determine which faction the datacard belongs to
		if($datacard == in_array($datacard, array("14:12358921", "14:12358706", "14:12359543", "14:12359042", "14:12358655", "14:12358722", "14:12358796", "14:12359544", "14:12359074", "14:12358808"))) {
			//Faction is Blue Star Engineering
			Tokens::updateAccessToken($db, "Tomas O`Cuinn"); //Grab new tokens
			$accessToken = Tokens::dbAccessToken($db, "Tomas O`Cuinn"); //Get refreshed access token
		} elseif($datacard == in_array($datacard, array("14:12337112", "14:12337111", "14:12358155", "14:12358156", "14:12337109", "14:12358154", "14:12337110", "14:12358868", "14:12337108", "14:12358153", "14:12358152", "14:12337107", "14:12358151"))) {
			//Faction is Blue Star Extractions
			Tokens::updateAccessToken($db, "Pursh Greene"); //Grab new tokens
			$accessToken = Tokens::dbAccessToken($db, "Pursh Greene"); //Get refreshed access token
		} else {
			echo "Unable to determine faction."; //Datacard GET does not match pre-set array.
			return;
		}

		$url = 'https://www.swcombine.com/ws/v2.0/datacard/'. $datacard .'/?access_token='. $accessToken; //Build URL with variables

		//For each production entity ID, send one POST
		foreach($id as $key => $value) {
			//Build array depending on how many uses are requested
			if($uses == "-1") {
				$postdata = http_build_query(array('production_entity_uid' => $value, 'unlimited' => "1")); //If assigns are unlimited
			} elseif($uses > 0) {
				$postdata = http_build_query(array('production_entity_uid' => $value, 'uses' => $uses)); //If assigned are not unlimited
			}
		
			$ch = curl_init(); //Initiate cURL session
			curl_setopt($ch, CURLOPT_URL, $url); //URL to fetch
			curl_setopt($ch, CURLOPT_HEADER, 0); //Include header in output
			curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata); //Data array to send in POST
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //Return page contents as string
			$result = curl_exec($ch); //Execute cURL session

			$httpRet = curl_getinfo($ch, CURLINFO_HTTP_CODE); //Get return code and potential error message
			if($httpRet == 200){
				echo "<p>Datacard successfully assigned to ID# ". $value ."</p>";
			} elseif($httpRet == 400){
				echo "Request failed, error code: ". $httpRet ." Bad Request: One or more of the provided parameters was invalid.";
			} elseif($httpRet == 401){
				echo "Request failed, error code: ". $httpRet ." Unauthorized: The character lacks access to this datacard.";
			} elseif($httpRet == 404){
				echo "Request failed, error code: ". $httpRet ." Not Found: The specified faction does not exist.";
			} else {
				echo "Request failed due to unknown error!";
			}
		}
	}
}
?>
