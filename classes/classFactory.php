<?php
	include_once './includes/dbh.inc.php';
	include_once './includes/datecalc.inc.php';
	include_once './includes/functions.inc.php';
	include_once 'classTokens.php';

//Handle production API
class Factory {
		
	//Load & display factories from API
	static function loadFactories() {
		//Establish connection to database
		$db = new Database();
		$db->connect();

		Tokens::updateAccessToken($db, "Tomas O`Cuinn"); //Updates access & refresh tokens through API

		//End connection to database
		$db->disconnect();
		unset($db);
		
		if($_GET['type'] == "factory") {
			$type = "factory"; //Determine entity type
			$urlType = "facilities"; //Determine API endpoint
		} elseif($_GET['type'] == "shipyard") {
			$type = "shipyard"; //Determine entity type
			$urlType = "stations"; //Determine API endpoint
		} else {
			echo "Incorrect entity type selection.";
			return; //Stop running if GET does not match these options
		};

		//Determine requested systen
		if($_GET['system'] == "dagu") {
			$system = "dagu"; //Determine system
		} elseif($_GET['system'] == "woldona") {
			$system = "woldona"; //Determine system
		} else {
			echo "Incorrect system selection."; 
			return; //Stop running if GET does not match these options
		};

		$entityID = self::determineIDs($type, $system); //Build array of entity IDs based on factory/station and system
		?>
		<table>
			<th style="text-align: left">Production Entity<br />ID#<br />Infotext</th>
			<th style="text-align: left">Production Status<br />Current Production<br />Time Remaining</th>
			<th style="text-align: left">Datacard Assignments</th>
		<?php

		self::pullFactories($entityID, $urlType); //Get and display factory/shipyard data from API

		?>
		</table>
		<?php
	}

	//Build array of entity IDs based on factory/station and system
	static function determineIDs(&$type, &$system) {
		if($type == "factory" && $system == "woldona") {
			//Build array with factory IDs in Woldona system
			return array("4:613310", "4:613311", "4:613313", "4:448090", "4:448092", "4:502611", "4:502922", "4:502918", "4:502616", "4:802612", "4:802616", "4:802617", "4:777137", "4:777147", "4:786869", "4:769415", "4:769512", "4:769522", "4:777129");
		} elseif($type == "factory" && $system == "dagu") {
			//Build array with factory IDs in Dagu system
			return array("4:4171079", "4:4171082", "4:4171103", "4:4171051", "4:4171084", "4:4171104", "4:4171053", "4:4171129", "4:4171133", "4:4171073", "4:4171132", "4:4171137", "4:4171235", "4:4171242", "4:4171487", "4:4171232", "4:4171243", "4:4171486", "4:4171217", "4:4171473", "4:4171476", "4:4171216", "4:4171474", "4:4171475", "4:4173449", "4:4173548", "4:4173547", "4:4173450", "4:4173549", "4:4173546", "4:4173568", "4:4173565", "4:4173564", "4:4173570", "4:4173560", "4:4173563", "4:4172443", "4:4172477", "4:4172488", "4:4172445", "4:4172489", "4:4172493", "4:4172536", "4:4172516", "4:4172508", "4:4172537", "4:4172519", "4:4172511", "4:4171533", "4:4171571", "4:4171576", "4:4171535", "4:4171569", "4:4171570", "4:4171543", "4:4171556", "4:4171562", "4:4171547", "4:4171554", "4:4171555", "4:4172772", "4:4172786", "4:4172984", "4:4172777", "4:4172791", "4:4172981", "4:4172798", "4:4172967", "4:4172974", "4:4172816", "4:4172968", "4:4172969", "4:4173048", "4:4173051", "4:4173106", "4:4173049", "4:4173635", "4:4173105", "4:4173063", "4:4173070", "4:4173095", "4:4173065", "4:4173067", "4:4173084", "4:4173681", "4:4173843", "4:4173839", "4:4173682", "4:4173844", "4:4173838", "4:4173684", "4:4173686", "4:4173849", "4:4173229", "4:4173685", "4:4173853");
		} elseif($type == "shipyard" && $system == "woldona") {
			//Build array with shipyards IDs in Woldona system
			return array("5:28398", "5:28396", "5:28397", "5:28395", "5:28400", "5:28399", "5:2284", "5:19432", "5:31418", "5:31413", "5:31415", "5:31412", "5:31432", "5:31419", "5:31431", "5:31410", "5:31381", "5:31379", "5:31380", "5:31377", "5:31399", "5:31382", "5:31375", "5:28404", "5:28408", "5:28405", "5:28406", "5:27211", "5:27214", "5:27212", "5:27213", "5:1212", "5:1617", "5:1094", "5:1093", "5:1175", "5:1747", "5:1466", "5:21430", "5:21615");
		} elseif($type == "shipyard" && $system == "dagu") {
			//Build array with shipyard IDs in Dagu system
			return array("5:34000", "5:34001", "5:34002", "5:33999", "5:34003", "5:34004", "5:33996", "5:33998", "5:17913", "5:17878", "5:17914", "5:17880", "5:31457", "5:31462", "5:31459", "5:31460", "5:9360", "5:32051", "5:32046", "5:32050", "5:32045", "5:32074", "5:32054", "5:32072", "5:32071", "5:9396", "5:32002", "5:32004", "5:32003", "5:32006", "5:32058", "5:32000", "5:32010", "5:32008", "5:9416");
		}
	}

	//Get and display factory/shipyard data from API
	static function pullFactories(&$entityID, &$urlType) {
		foreach($entityID as $urlID) {
			$url = 'https://www.swcombine.com/ws/v2.0/inventory/'. $urlType .'/'. $urlID .'?&access_token='. $_SESSION['access_token']; //Build endpoint URL with variables
			$ch = curl_init(); //Initiate cURL session
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //Return page contents as string
			curl_setopt($ch, CURLOPT_URL, $url); //Set built URL
			$result = curl_exec($ch); //Execute cURL session
			$xml = simplexml_load_string($result); //Convert string into object

			foreach($xml->entity as $factory) {
				$factoryName = $factory->name; //Entity name
				$factoryID = str_replace(array("4:", "5:"), '', $factory->uid); //Entity ID
				$factoryInfoText = $factory->infotext; //Entity infotext

				echo "<tr><td style='text-align: left'><span style='color: gold'>". $factoryName ."</span><br />ID# ". $factoryID ."<br />". $factoryInfoText ."</td>";

				//Check if entity is currently producing another entity
				if($factory->actions['count'] > 0) {
					//If factory is producing, show "active" & what it's tooled to
					$producingType = $factory->tooledto; //What the entity is tooled to produce
					$producingQuantity = $factory->actions->action->quantity; //How many units are being produced
					$producingDaysRemaining = numForm($factory->actions->action->delay->remaining / 86400, 0); //How many days remain in production
					$producingCompleteDate = formatdateYD(time() + $factory->actions->action->delay->remaining); //Calculate production completion date
					echo "<td style='text-align: left'><span style='color: lightgreen; font-weight: bold'>Active</span>";
					echo "<br />(". $producingQuantity .") ". $factory->tooledto ."<br />". $producingDaysRemaining ." days (". $producingCompleteDate .")</td>";
				} else {
					//If factory is not producing, show "inactive"
					echo "<td style='text-align: left'><span style='color: orange; font-weight: bold'>Inactive</span></td>";
				}

					echo "<td style='text-align: left'>";
				if($factory->assigneddcs['count'] > 0) {
					foreach($factory->assigneddcs->card as $datacard) {
						$datacardType = $datacard->type; //Which datacards are assigned
						$datacardQty = str_replace("-1", "∞", $datacard->uses); //Number of datacard uses assigned
							echo "(". $datacardQty .") ". $datacardType ."<br />";
					}
				}
				echo "</td></tr>";
			}
		}
	}
};
?>
