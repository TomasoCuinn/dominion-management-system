<?php
include_once './includes/dbh.inc.php';
include_once './includes/datecalc.inc.php';

//Handle GNS functions
    class GNS {

        //Counts total rows in database flashnews table
        static function countFlashnewsDatabase(&$db) {
            $stmt = $db->connection->prepare("SELECT COUNT(*) FROM `flashnews`"); //Build query
            $stmt->execute(); //Execute query
            $stmt->bind_result($result); //Bind column to variable
            $stmt->fetch(); //Fetch results
            return $result; //Return total rows in database table
	    }

        //Counts total flashnews entries on SWC API
        static function countFlashnewsCombine() {
            $ch = curl_init(); //Initiate cURL session
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //Return page contents as string
            curl_setopt($ch, CURLOPT_URL, 'https://www.swcombine.com/ws/v2.0/news/gns/auto'); //URL to fetch
            $result = curl_exec($ch); //Execute cURL session
            $xml = simplexml_load_string($result); //Convert string into object
            $rowsAPI = $xml->newsitems['total']; //Count flashnews entries on SWC
            return floatval($rowsAPI); //Converts object into string and returns number
	    }

        //Insert 50 latest GNS flashnews into database
    	static function pullNewFlashnews(&$db) {
            $ch = curl_init(); //Initiate cURL session
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //Return page contents as string
            curl_setopt($ch, CURLOPT_URL, 'https://www.swcombine.com/ws/v2.0/news/gns/auto?&item_count=50'); //URL to fetch
            $result = curl_exec($ch); //Execute cURL session
            $xml = simplexml_load_string($result); //Convert string into object

            foreach($xml->newsitems->newsitem as $row) {
                //Check if each flashnews ID already exists in database
                $stmt = $db->connection->prepare("SELECT * FROM `flashnews` WHERE `id` = ?"); //Build query
                $stmt->bind_param("s", $row['id']); //Bind variables to query parameters
                $stmt->execute(); //Execute query
                $result = $stmt->get_result(); //Gets result set from query
                $resultCheck = mysqli_num_rows($result); //Counts rows in result set

                //If ID does not already exist, insert each one
                if($resultCheck == 0) {
                    $gnsID = ($row['id']);

                    $ch = curl_init(); //Initiate cURL session
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //Return page contents as string
                    curl_setopt($ch, CURLOPT_URL, 'https://www.swcombine.com/ws/v2.0/news/gns/'.$gnsID.''); //URL to fetch & pass ID variable
                    $result = curl_exec($ch); //Execute cURL session
                    $xml2 = simplexml_load_string($result); //Convert string into object

                    $db->connection->begin_transaction(); //Start SQL qeuery
                    $stmt = $db->connection->prepare("INSERT INTO `flashnews` (`id`, `year`, `day`, `hour`, `minute`, `second`, `timestamp`, `title`, `body`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)"); //Build query

                    //Insert data into datacards table
                    foreach($xml2->newsitem as $row) {
                        $title = $row->title;
                        $body = $row->body;
                        $year = $row->posted->years;
                        $day = $row->posted->days;
                        $hour = $row->posted->hours;
                        $min = $row->posted->mins;
                        $sec = $row->posted->secs;
                        $timestamp = $row->posted->timestamp;
                        $id = $row->id;
						$stmt->bind_param("sssssssss", $id, $year, $day, $hour, $sec, $min, $timestamp, $title, $body); //Bind variables to query parameters
                        $stmt->execute(); //Execute query for each entry
                    }
                    $db->connection->commit(); //Commit to database
                }
            }
	    }

        //Compare database entries against SWC entries, inserting new records if a different exists
        static function compareEntries(&$db){
            //Check and display how many rows the database contains
			$rowsDatabase = self::countFlashnewsDatabase($db);
			$rowsCombine = self::countFlashnewsCombine();

			//If SWC has more entries than database, grab and insert them
			if($rowsCombine > $rowsDatabase) {
				self::pullNewFlashnews($db);
			}
            echo "<p>This database has ". number_format($rowsDatabase) ." entries while Combine has ". number_format($rowsCombine) ." entries.</p>
            <p>The latest 50 entries are displayed by default, but a search will display all entries found.</p>";
        }

        //Perform a user search of database table
        static function userSearch(&$db, &$input) {
			//Sanitize user input
			$search1 = str_replace(array('\'', '"', ',', ';', '<', '>', '&'), '', $input);
			$search = filter_var($search1, FILTER_SANITIZE_STRING);
			$searchFiltered = mysqli_real_escape_string($db->connection, $search);
			$searchWildcard = "%". $search ."%";
			date_default_timezone_set('America/Los_Angeles');
			$currentTimestamp = date("Y-m-d H:i:s");

			//Insert new search record into database
			$db->connection->begin_transaction();
			$stmt = $db->connection->prepare("INSERT INTO `flashnewsSearches` (`timestamp`, `search`) VALUES (?, ?)");
			$stmt->bind_param("ss", $currentTimestamp, $searchFiltered);
			$stmt->execute();
			$db->connection->commit();

			//Perform user search in database
			$db->connection->begin_transaction();
			$stmt = $db->connection->prepare("SELECT * FROM flashnews WHERE `title` LIKE ? OR `body` LIKE ? ORDER BY `id` DESC");
			$stmt->bind_param("ss", $searchWildcard, $searchWildcard);
			$stmt->execute();
			$result = $stmt->get_result();
			$resultCheck = mysqli_num_rows($result);

			//If table is not empty, display all search results pulled from database
			if ($resultCheck > 0) {
				echo "<p><span style='color: gold'>Your search generated <span style='font-weight: bold'>". number_format($resultCheck) ."</span> records.</span></p>";
				?>
				<table class='gns'>
					<tr class='gns'>
						<th class='date'>Date</th>
						<th class='gns'>Title</th>
						<th class='gns'>Body</th>
					</tr>
				<?php
				while ($row = mysqli_fetch_assoc($result)) {
					$date = formatDateYD($row['timestamp']);
					$title = $row['title'];
					$body = str_replace('href="/', 'href="https://www.swcombine.com/', $row['body']);
					echo "<tr class='gns'>";
						echo "<td class='gns'>". $date ."</td>";
						echo "<td class='gns'>". $title ."</td>";
						echo "<td class='gns'>". $body ."</td>";
					echo "</tr>";
				}
				?>
				</table>
				<?php
            } else {
				echo "Failed to pull GNS records.";
			}
        }

        //Pull all flashnews from database
        static function allRecords(&$db) {
			$sql = "SELECT * FROM `flashnews` ORDER BY `id` DESC";
			$result = mysqli_query($db->connection, $sql);
			$resultCheck = mysqli_num_rows($result);

			//If table is not empty, display 50 results pulled from database
			if($resultCheck > 0) {
				?>
				<table class='gns'>
					<tr class='gns'>
						<th class='gns'>Date</th>
						<th class='gns'>Title</th>
						<th class='gns'>Body</th>
					</tr>
				<?php
				while ($row = mysqli_fetch_assoc($result)) {
					$date = formatDateYD($row['timestamp']);
					$title = $row['title'];
					$body = str_replace('href="/', 'href="https://www.swcombine.com/', $row['body']);
					echo "<tr class='gns'>";
						echo "<td class='gns'>". $date ."</td>";
						echo "<td class='gns'>". $title ."</td>";
						echo "<td class='gns'>". $body ."</td>";
					echo "</tr>";
				}
				?>
				</table>
			<?php
			} else {
				echo "Failed to pull GNS records.";
			}
        }
    
        //If user does not request all records or perform search, pull 50 latest records from database
        static function defaultRecords(&$db) {
        	//Pull 50 latest flashnews from database
			$sql = "SELECT * FROM `flashnews` ORDER BY `id` DESC LIMIT 50";
			$result = mysqli_query($db->connection, $sql);
			$resultCheck = mysqli_num_rows($result);

    		//If table is not empty, display 50 results pulled from database
			if ($resultCheck > 0) {
				?>
				<table class='gns'>
					<tr class='gns'>
						<th class='gns'>Date</th>
						<th class='gns'>Title</th>
						<th class='gns'>Body</th>
					</tr>
				<?php
				while ($row = mysqli_fetch_assoc($result)) {
					$date = formatDateYD($row['timestamp']);
					$title = $row['title'];
					$body = str_replace('href="/', 'href="https://www.swcombine.com/', $row['body']);
	
					echo "<tr class='gns'>";
						echo "<td class='gns'>". $date ."</td>";
						echo "<td class='gns'>". $title ."</td>";
						echo "<td class='gns'>". $body ."</td>";
					echo "</tr>";
				}
				?>
				</table>
				<?php
			} else {
				echo "Failed to pull GNS records.";
			}
        }
    }
?>