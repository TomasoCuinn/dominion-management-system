<?php
	include_once './includes/dbh.inc.php';

//Handle income data examination
	class Income {
		public $incomeGeneric;
		public $incomeReligious;
		public $incomeTrading;
		public $incomeMedical;
		public $planetIncomeD1;
		public $planetIncomeD2;
		public $planetIncomeD3;
		public $planetIncomeD4;
		public $planetIncomeW1;
		public $planetIncomeW2;
		public $planetIncomeW3;
		public $planetIncomeW4;
		public $planetIncomeW5;
		public $planetIncomeW6;
		public $planetIncomeW7;
		public $systemIncomeDagu;
		public $systemIncomeWoldona;
		public $incomeBSD;
		public $countWeeksAll;
		public $countWeeksY20;
		public $countWeeksY21;
		public $countWeeksY22;
		public $countWeeksY23;
		public $countWeeksY24;
		public $sumIncomeYAll;
		public $sumIncomeY20;
		public $sumIncomeY21;
		public $sumIncomeY22;
		public $sumIncomeY23;
		public $sumIncomeY24;

		//Pull income data from database and examine data by populating properties
		function __construct() {
			$db = new Database();

			$sqlArray = array(
				"SELECT COUNT(`Date`) FROM `incomeOverTime` WHERE `date` LIKE '2%'", //Array position 0-0
				"SELECT COUNT(`Date`) FROM `incomeOverTime` WHERE `date` LIKE '20%'", //Array position 1-0
				"SELECT COUNT(`Date`) FROM `incomeOverTime` WHERE `date` LIKE '21%'", //Array position 2-0
				"SELECT COUNT(`Date`) FROM `incomeOverTime` WHERE `date` LIKE '22%'", //Array position 3-0
				"SELECT COUNT(`Date`) FROM `incomeOverTime` WHERE `date` LIKE '23%'", //Array position 4-0
				"SELECT COUNT(`Date`) FROM `incomeOverTime` WHERE `date` LIKE '24%'", //Array position 5-0
				"SELECT SUM(`amount`) from income WHERE `planetName` = 'Dagu'", //Array position 6-0
				"SELECT SUM(`amount`) from income WHERE `planetName` = 'Dagu II'", //Array position 7-0
				"SELECT SUM(`amount`) from income WHERE `planetName` = 'Dagu III'", //Array position 8-0
				"SELECT SUM(`amount`) from income WHERE `planetName` = 'Dagu IV'", //Array position 9-0
				"SELECT SUM(`amount`) from income WHERE `planetName` = 'Woldona I'", //Array position 10-0
				"SELECT SUM(`amount`) from income WHERE `planetName` = 'Woldona II'", //Array position 11-0
				"SELECT SUM(`amount`) from income WHERE `planetName` = 'Woldona III'", //Array position 12-0
				"SELECT SUM(`amount`) from income WHERE `planetName` = 'Woldona IV'", //Array position 13-0
				"SELECT SUM(`amount`) from income WHERE `planetName` = 'Woldona V'", //Array position 14-0
				"SELECT SUM(`amount`) from income WHERE `planetName` = 'Woldona VI'", //Array position 15-0
				"SELECT SUM(`amount`) from income WHERE `planetName` = 'Woldona VII'", //Array position 16-0
				"SELECT SUM(`amount`) from income WHERE `planetName` LIKE 'Dagu%'", //Array position 17-0
				"SELECT SUM(`amount`) from income WHERE `planetName` LIKE 'Woldona%'", //Array position 18-0
				"SELECT SUM(`amount`) from income WHERE `manager` LIKE 'Blue Star%'", //Array position 19-0
				"SELECT SUM(`amount`) from income WHERE `manager` = 'Alissma'", //Array position 20-0
				"SELECT SUM(`amount`) from income WHERE `manager` = 'Fairwind Exotics'", //Array position 21-0 
				"SELECT SUM(`amount`) from income WHERE `manager` = 'Uukaablian MedCorp'", //Array position 22-0
				"SELECT SUM(`income` + `tax`) FROM incomeOverTime WHERE `date` LIKE '2%'", //Array position 23-0
				"SELECT SUM(`income` + `tax`) FROM incomeOverTime WHERE `date` LIKE '20.%'", //Array position 24-0
				"SELECT SUM(`income` + `tax`) FROM incomeOverTime WHERE `date` LIKE '21.%'", //Array position 25-0
				"SELECT SUM(`income` + `tax`) FROM incomeOverTime WHERE `date` LIKE '22.%'", //Array position 26-0
				"SELECT SUM(`income` + `tax`) FROM incomeOverTime WHERE `date` LIKE '23.%'", //Array position 27-0
				"SELECT SUM(`income` + `tax`) FROM incomeOverTime WHERE `date` LIKE '24.%'", //Array position 28-0
			); //Insert all queries into array

			$sql = implode(';', $sqlArray); //Implode query array into string

			$db->connection->multi_query($sql);
			do {
				if($result = $db->connection->store_result()) { //Store result set
					while($row = $result->fetch_row()) {$array[] = $row;} //Assign result to array
				}
			} while($db->connection->next_result()); //Build array as long as there is new data

			$this->countWeeksAll = $array['0']['0'];
			$this->countWeeksY20 = $array['1']['0'];
			$this->countWeeksY21 = $array['2']['0'];
			$this->countWeeksY22 = $array['3']['0'];
			$this->countWeeksY23 = $array['4']['0'];
			$this->countWeeksY24 = $array['5']['0'];
			$this->planetIncomeD1 = $array['6']['0'];
			$this->planetIncomeD2 = $array['7']['0'];
			$this->planetIncomeD3 = $array['8']['0'];
			$this->planetIncomeD4 = $array['9']['0'];
			$this->planetIncomeW1 = $array['10']['0'];
			$this->planetIncomeW2 = $array['11']['0'];
			$this->planetIncomeW3 = $array['12']['0'];
			$this->planetIncomeW4 = $array['13']['0'];
			$this->planetIncomeW5 = $array['14']['0'];
			$this->planetIncomeW6 = $array['15']['0'];
			$this->planetIncomeW7 = $array['16']['0'];
			$this->systemIncomeDagu = $array['17']['0'];
			$this->systemIncomeWoldona = $array['18']['0'];
			$this->incomeGeneric = $array['19']['0'];
			$this->incomeReligious = $array['20']['0'];
			$this->incomeTrading = $array['21']['0'];
			$this->incomeMedical = $array['22']['0'];
			$this->sumIncomeAll = $array['23']['0'];
			$this->sumIncomeY20 = $array['24']['0'];
			$this->sumIncomeY21 = $array['25']['0'];
			$this->sumIncomeY22 = $array['26']['0'];
			$this->sumIncomeY23 = $array['27']['0'];
			$this->sumIncomeY24 = $array['28']['0'];

			$db->disconnect(); //End connection to database
		}
	}
?>
