<?php
	include_once './includes/dbh.inc.php';
	include_once './includes/functions.inc.php';
	include_once 'classTime.php';

//Handle access & refresh tokens
class Tokens {
	static protected $clientID = 'REMOVED';
	static protected $clientSecret = 'REMOVED';

	//Pulls Access Token from database
	static function dbAccessToken(&$db, $handle) {
		$sql = $db->connection->prepare("SELECT `accessToken` FROM accounts WHERE `handle` LIKE ?");
		$sql->bind_param("s", $handle);
		$sql->execute();
		$sql->bind_result($result);
		$sql->fetch();
		return $result;
	}

	//Pulls Refresh Token from database
	static function dbRefreshToken(&$db, $handle) {
		$sql = $db->connection->prepare("SELECT `refreshToken` FROM accounts WHERE `handle` LIKE ?");
		$sql->bind_param("s", $handle);
		$sql->execute();
		$sql->bind_result($result);
		$sql->fetch();
		return $result;
	}

	//Updates access/refresh tokens for all accounts
	static function updateAllTokens() {
		//Establish connection to database
		$db = new Database();
		$db->connect();

		$sql = "SELECT `handle` FROM `accounts`";
		$result = mysqli_query($db->connection, $sql);
		
		while($row = mysqli_fetch_assoc($result)) {
			$update = Tokens::updateAccessToken($db, $row['handle']);
			if($update == "Success") {
				echo "<p><span style='color: lightgreen; font-weight: bold'>Successfully updated tokens for ". $row['handle'] .".</span></p>"; //Tokens were not refreshed
			}
		}

		//End connection to database
		$db->disconnect();
		unset($db);
	}

	//Update your own session tokens on page load, in case they've changed
	static function updateOwnTokens($handle) {
		//Establish connection to database
		$db = new Database();
		$db->connect();

		//Pull tokens from database
		$updatedAccessToken = self::dbAccessToken($db, $handle);
		$updatedRefreshToken = self::dbRefreshToken($db, $handle);

		//Update session variables with new tokens
		$_SESSION['access_token'] = $updatedAccessToken;
		$_SESSION['refresh_token'] = $updatedRefreshToken;

		//End connection to database
		$db->disconnect();
		unset($db);
	}

	//Update Refresh Token
	static function updateAccessToken(&$db, $handle) {
		//Define required variables
		$refreshTokenOld = self::dbRefreshToken($db, $handle);		
		$url = "https://www.swcombine.com/ws/oauth2/token/";

		//Build query from variables
		$postdata = http_build_query(
			array(
				'client_id' => self::$clientID,
				'client_secret' => self::$clientSecret,
				'refresh_token' => $refreshTokenOld,
				'grant_type' => 'refresh_token',
			)
		);

		//Convert query into array
		$options = array(
			'http' => array(
				'method' => 'POST',
				'header' => 'Content-Type: application/x-www-form-urlencoded',
				'content' => $postdata
			)
		);

		$ch = curl_init(); //Initiate cURL session
		curl_setopt($ch, CURLOPT_URL, $url); //URL to fetch
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata); //Data array to send in POST
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //Return page contents as string
		$result = curl_exec($ch); //Execute cURL session
		$result = simplexml_load_string($result);

		if($result){
			//Set new access/refresh tokens into variables
			$accessTokenNew = (string) $result->access_token;
			$refreshTokenNew = (string) $result->refresh_token;

			if(!$accessTokenNew) {
				echo "<p><span style='color: orange; font-weight: bold'>Failed to update tokens for ". $handle .".</span></p>"; //Tokens were not refreshed
				return;
			}

			//If updating own tokens, update session variables too
			if($handle == $_SESSION['handle']) {
				$_SESSION['access_token'] = (string) $result->access_token;
				$_SESSION['refresh_token'] = (string) $result->refresh_token;
			}

			//Insert new access/refresh tokens into database
			if($accessTokenNew) {
				$db->connection->begin_transaction();
				$stmt = $db->connection->prepare("UPDATE `accounts` SET `accessToken` = ?, `refreshToken` = ? WHERE `handle` = ?");
				$stmt->bind_param("sss", $accessTokenNew, $refreshTokenNew, $handle);
				$stmt->execute();
				$db->connection->commit();
				
				$validate = "Success"; //Validate that new tokens were refreshed
				return $validate;
			}
		}
	}
}
