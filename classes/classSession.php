<?php
	include_once './includes/dbh.inc.php';
	include_once 'classMembers.php';

//Handle user session
class Session {
	protected $handle;
	protected $avatar;
	protected $faction;
	protected $accessToken;
	protected $refreshToken;

	function __construct() {
		$this->handle = $_SESSION['handle'];
		$this->avatar = $_SESSION['avatar'];
		$this->faction = $_SESSION['faction'];
		$this->accessToken = $_SESSION['access_token'];
		$this->refreshToken = $_SESSION['refresh_token'];
	}

	//Get handle
	function getHandle() {
		$returnHandle = $this->handle;
		return $returnHandle;
	}

	//Get avatar
	function getAvatar() {
		$returnAvatar = $this->avatar;
		return $returnAvatar;
	}

	//Get faction
	function getFaction() {
		$returnFaction = $this->faction;
		return $returnFaction;
	}

	//Get access token
	function getAccessToken() {
		$returnAccess = $this->accessToken;
		return $returnAccess;
	}

	//Get refresh token
	function getRefreshToken() {
		$returnRefresh = $this->refreshToken;
		return $returnRefresh;
	}

	//Check if user has DMS access
	function accessCheck($db, $handle) {
		//Check if access granted from accounts table
		$sql = $db->connection->prepare("SELECT `access` from `accounts` WHERE `handle` LIKE ?");
		$sql->bind_param("s", $handle);
		$sql->execute();
		$sql->bind_result($result);
		$sql->fetch();
		if($result > 0) {
			$_SESSION['accessCheck'] = "Access Granted";
		} else {
			header('Location: https://swc-bsd.com/logout.php');
		}
	}

	//Create account and login record upon login
	function loginRecord($db, $handle, $faction, $accessToken, $refreshToken) {
		//Create new account in login, or update faction if account already exists
		$db->connection->begin_transaction();
		$stmt = $db->connection->prepare(
		"INSERT INTO `accounts` (`handle`, `faction`, `access`, `accessToken`, `refreshToken`) VALUES (?, ?, ?, ?, ?) ON DUPLICATE KEY UPDATE `faction` = ?, `accessToken` = ?, `refreshToken` = ?");
		$stmt->bind_param("ssssssss", $handle, $faction, $access, $accessToken, $refreshToken, $faction, $accessToken, $refreshToken);
		$access = "0";

		//Create a new login record
		$db->connection->begin_transaction();
		$stmt2 = $db->connection->prepare("INSERT INTO `logins` (`handle`, `faction`) VALUES (?, ?)");
		$stmt2->bind_param("ss", $handle, $faction);

		//Execute both SQL queries
		$stmt->execute();
		$stmt2->execute();
		$db->connection->commit();
	}

	//Check if user has 'read datacards' faction priv
	function checkFactionPrivReadDC() {
		//Check if user has "faction_datacards_read" faction priv
		$url = 'https://www.swcombine.com/ws/v2.0/character/'. $this->handle .'/privileges/datacard/view/?access_token='. $this->accessToken;
		$ch = curl_init(); //Initiate cURL session
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //Return page contents as string
		curl_setopt($ch, CURLOPT_URL, $url); //URL to fetch
		$result = curl_exec($ch); //Execute cURL session
		$xml = simplexml_load_string($result); //Convert string into object
		$privCheck = (string) $xml->privilege; //Field returns "true" or "false"

		//If user can read faction DCs, set SESSION variable
		if($privCheck == "true") {
			$_SESSION['privDC'] = "Set";
		}
	}

	//Check if user has 'view_memberlist' faction priv
	function checkFactionPrivMemberlist(&$db) {
		//Check if user can view faction memberlist and, if yes, insert into database
		$url = 'https://www.swcombine.com/ws/v2.0/character/'. $this->handle .'/privileges/members/view_memberlist/?access_token='. $this->accessToken;

		$ch = curl_init(); //Initiate cURL session
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //Return page contents as string
		curl_setopt($ch, CURLOPT_URL, $url); //URL to fetch
		$result = curl_exec($ch); //Execute cURL session
		$xml = simplexml_load_string($result); //Convert string into object
		$privCheck = (string) $xml->privilege; //Field returns "true" or "false"

		if($privCheck == "true") {
			//If user has priv, insert faction members into database
			Members::insertMembers($db, $_SESSION['faction'], $_SESSION['access_token']);
		}
	}
	};
?>
