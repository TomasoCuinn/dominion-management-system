<?php
	session_start();
	include_once 'includes/dbh.inc.php';
	include_once 'includes/functions.inc.php';
	include_once 'classes/classSession.php';
	include_once 'classes/classTokens.php';
	$start_time = microtime(true);
?>

<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="description" content="Blue Star Dominion Management System">
<meta name="author" content="Tomas o`Cuinn">
<meta name="copyright" content="Blue Star Dominion">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Dominion Management System</title>
<link rel="stylesheet" href="style.css">
<link rel="apple-touch-icon" sizes="180x180" href="/images/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="/images/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="/images/favicon-16x16.png">
<link rel="manifest" href="/site.webmanifest">
</head>
<body>

<?php
//If authorization code is obtained from SWC API, exchange it for access token
if($_GET['code']) {
	$url = "https://www.swcombine.com/ws/oauth2/token/";

	$postdata = http_build_query(
		array(
			'code' => filter_var($_GET['code'], FILTER_SANITIZE_SPECIAL_CHARS),
			'client_id' => 'REMOVED',
			'client_secret' => 'REMOVED',
			'redirect_uri' => "https://swc-bsd.com/oauth.php",
			'grant_type' => 'authorization_code',
			'access_type' => 'offline'
		)
	);

	$headers[] = 'Content-length: '.strlen($postdata);

	$ch = curl_init(); //Initiate cURL session
	curl_setopt($ch, CURLOPT_URL, $url); //URL to fetch
	curl_setopt($ch, CURLOPT_HEADER, 0); //Include header in output
	curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata); //Data array to send in POST
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers); //Array of header fields
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //Return page contents as string
	
	$result = curl_exec($ch); //Execute cURL session
	$result = simplexml_load_string($result); //Convert string into object
	$result = json_encode($result); //Encode array into JSON object
	$result = json_decode($result); //Decode JSON object into PHP object

	curl_close($ch); //Close cURL session

	$_SESSION['access_token'] = (string) $result->access_token; //Set access token as session variable
	$_SESSION['refresh_token'] = (string) $result->refresh_token; //Set refresh token as session variable

	//If access token is obtained, pull character data
	if($result){
		//Use access token to pull character data
		$url = 'https://www.swcombine.com/ws/v2.0/character/?access_token='. $result->access_token .'';
		$ch = curl_init(); //Initiate cURL session
		curl_setopt($ch, CURLOPT_URL, $url); //URL to fetch
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //Return page contents as string
		$result = curl_exec($ch); //Execute cURL session
		$result = simplexml_load_string($result); //Convert string into object
		$result = json_encode($result); //Encode array into JSON object
		$result = json_decode($result); //Decode JSON object into PHP object
		curl_close($ch); //Close cURL session

		$_SESSION['handle'] = (string) $result->character->name; //Set session variable based on character data
		$_SESSION['avatar'] = (string) $result->character->image; //Set session variable based on character data
		$_SESSION['faction'] = (string) $result->character->faction; //Set session variable based on character data

		//If verified through API, create account and login record in database
		if(isset($_SESSION['handle'])) {
			//Establish connection to database
			$db = new Database();
			$db->connect();

			$session = new Session();
			$session->accessCheck($db, $_SESSION['handle']); //Verify website access through accounts table
			$session->loginRecord($db, $_SESSION['handle'], $_SESSION['faction'], $_SESSION['access_token'], $_SESSION['refresh_token']); //Create a user login record
			$session->checkFactionPrivReadDC(); //Check if user has 'read datacards' faction priv
			$session->checkFactionPrivMemberlist($db); //Pull faction memberlist if user has 'view memberlist' faction priv

			//End connection to database
			$db->disconnect();
			unset($db);

			header('Location: https://swc-bsd.com/index.php'); //Forward to index if authCode & accessToken retrieved & handle set
		} else {header('Location: https://swc-bsd.com/logout.php');} //If handle is not set, send to logout (destroy session)
	} else {header('Location: https://swc-bsd.com/logout.php');} //If no access token received, send to logout (destroy session)
} else {header('Location: https://swc-bsd.com/logout.php');} //If no authentication code received, send to logout (destroy session)
?>
</body>
</html>
