<?php
include_once '../includes/dbh.inc.php';
include_once '../includes/functions.inc.php';
session_start()
?>

<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="description" content="Blue Star Dominion Management System">
<meta name="author" content="Tomas o`Cuinn">
<meta name="copyright" content="Blue Star Dominion">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Dominion Management System</title>
<link rel="stylesheet" href="../style.css">
<link rel="apple-touch-icon" sizes="180x180" href="../images/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="../images/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="../images/favicon-16x16.png">
<link rel="manifest" href="../site.webmanifest">
</head>
<body>
	<div class="grid-container">
		<div class="item1">
			<h2>Dominion Management System</h2>
		</div>
		<div class="item2">
			<a class="no-list-style" href="index.php">Home</a>
			<a class="no-list-style" href="payroll.php">Payroll</a>
			<a class="no-list-style" href="income.php">Income</a>
		</div>

		<div class="item3">
		<?php
		//Establish connection to database
		$db = new Database();
		$db->connect();

		$truncate = "TRUNCATE TABLE `income`";
		mysqli_multi_query($db->connection, $truncate);

		$xml = simplexml_load_file("../FI.xml");
		if(!$xml) {
			die("Error: Cannot create object");
		}

		$db->connection->begin_transaction();
		$stmt = $db->connection->prepare("INSERT INTO income (name, buildingID, typeName, planetName, manager, amount) VALUES (?, ?, ?, ?, ?, ?)");

		foreach ($xml->channel->item as $row) {
			$name = str_replace("'", '', ($row->name));
			$buildingID = ($row->buildingID);
			$typeName = ($row->typeName);
			$planetName = ($row->planetName);
			$manager = ($row->manager);
			$amount = ($row->amount);
			$stmt->bind_param("ssssss", $name, $buildingID, $typeName, $planetName, $manager, $amount);
			$stmt->execute();
		}
		$db->connection->commit();

		//End connection to database
		$db->disconnect();
		unset($db);

		//Send user back to income.php
		header('Location: https://swc-bsd.com/income.php');
		?>
		</div>
		<div class="item4">
			<div class="navbarbot">
				<a href="https://www.swcombine.com/" target="_blank">SWC</a>
			</div>
			<div class="navbarbot">
				<a href="https://discord.gg/kQ2Tusd" target="_blank">Discord</a>
			</div>
			<div class="navbarbot">
				<a href="http://ionetwork3.com" target="_blank">ION</a>
			</div>
			<div class="navbarbot">
				<a href="http://swcpiston.com" target="_blank">PISTON</a>
			</div>
		</div>
	</div>
</body>
</html>