<?php
	session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="description" content="Blue Star Dominion Management System">
<meta name="author" content="Tomas o`Cuinn">
<meta name="copyright" content="Blue Star Dominion">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Dominion Management System</title>
<link rel="stylesheet" href="style.css">
<link rel="apple-touch-icon" sizes="180x180" href="/images/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="/images/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="/images/favicon-16x16.png">
<link rel="manifest" href="/site.webmanifest">
</head>
<body>
	<?php
	if (isset($_SESSION['handle'])) {
		//If user is logged in, send to index
		echo "Welcome, ". $_SESSION['handle'] ."!";
		header('Location: https://swc-bsd.com/index.php');
	} else {
		//If user is not logged in, use SWC API to log in
		$scopes = [
			'character_auth',
			'character_read',
			'character_privileges',
			'character_events',
			'faction_all',
			'FACTION_INV_CITIES_ALL',
			'FACTION_INV_DROIDS_ALL',
			'FACTION_INV_FACILITIES_ALL',
			'faction_inv_facilities_read',
			'FACTION_INV_ITEMS_ALL',
			'FACTION_INV_MATERIALS_ALL',
			'FACTION_INV_NPCS_ALL',
			'FACTION_INV_PLANETS_ALL',
			'FACTION_INV_SHIPS_ALL',
			'FACTION_INV_STATIONS_ALL',
			'faction_inv_stations_read',
			'FACTION_INV_VEHICLES_ALL',
			'FACTION_CREDITS_READ',
			'FACTION_CREDITS_WRITE'
		];
		?>
		<p>
		<button class="Login" onclick="window.location.href='https://www.swcombine.com/ws/oauth2/auth/?scope=<?php echo implode(' ', $scopes) ?>&redirect_uri=https%3A%2F%2Fswc-bsd.com/oauth.php&response_type=code&client_id=a1a54858c640a56d8428160ec802bdec14f06b10&access_type=offline&renew_previously_granted=yes';">
			Log in with SWC API
		</button>
		</p>
		<?php
	}
	?>
</body>
</html>
