<?php
	session_start();
	include_once 'classes/classDatabase.php';
	include_once 'classes/classSession.php';
	include_once 'classes/classTokens.php';
	$start_time = microtime(true);
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="description" content="Blue Star Dominion Management System">
<meta name="author" content="Tomas o`Cuinn">
<meta name="copyright" content="Blue Star Dominion">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Dominion Management System</title>
<link rel="stylesheet" href="style.css">
<link rel="apple-touch-icon" sizes="180x180" href="/images/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="/images/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="/images/favicon-16x16.png">
<link rel="manifest" href="/site.webmanifest">
</head>
<body>
	<div class="grid-container">
		<div class="item1">
			<h2>Dominion Management System</h2>
		</div>
		<div class="item2"> <!--Top Navigation-->
			<?php
			if(!isset($_SESSION['accessCheck'])) {
				//If accessCheck is not set, send user to logout (destroy session)
				//accessCheck session variable is only set when user has access in the accounts db table
				header('Location: https://swc-bsd.com/logout.php');
			}
			Tokens::updateOwnTokens($_SESSION['handle']); //Update user session tokens, in case they've changed

			echo "<p>Logged in as <font color=gold><strong>". $_SESSION['handle'] ."</font></strong>.";
			?>
			<button onclick="window.location.href='https://swc-bsd.com/logout.php'">Log out</button>
			</p>
			<a class="no-list-style" href="index.php">Home</a>
			<a class="no-list-style" href="members.php">Members</a>
			<a class="no-list-style" href="datacards.php">DCs</a>
			<a class="no-list-style" href="production.php">Production</a>
			<a class="no-list-style" href="income.php">Income</a>
		</div> <!--Top Navigation-->
