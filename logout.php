<?php
	session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="description" content="Blue Star Dominion Management System">
<meta name="author" content="Tomas o`Cuinn">
<meta name="copyright" content="Blue Star Dominion">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Dominion Management System</title>
<link rel="stylesheet" href="style.css">
<link rel="apple-touch-icon" sizes="180x180" href="/images/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="/images/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="/images/favicon-16x16.png">
<link rel="manifest" href="/site.webmanifest">
</head>

<?php
session_unset(); //Remove all session variables
session_destroy(); //Destroy session
header('Location: https://swc-bsd.com/login.php'); //Sent user to login page
?>