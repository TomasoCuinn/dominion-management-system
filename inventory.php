<?php
	require 'header.php';
	include_once 'classes/classInventory.php';
?>
	<div class="item3">
		<h3>Inventory Mangement</h3>

		<form>
		<p><label for="faction">Faction:</label>
			<select name="faction" id="faction">
				<option value=""></option>
				<option value="Blue Star Enforcement">Enforcement</option>
				<option value="Blue Star Engineering">Engineering</option>
				<option value="Blue Star Extractions">Extractions</option>
			</select></p>

			<p><label for="entity_type">Entity Type:</label>
			<select name="entity_type" id="entity_type">
				<option value=""></option>
				<option value="ships">Ship</option>
				<option value="stations">Station</option>
				<option value="facilities">Facility</option>
			</select></p>

			<p><label for="id">Entity ID#: </label>
			<input type="id" id="id" name="id" value="<?php if(isset($_GET['id'])) {echo $_GET['id'];}; ?>"></p>

			<p><label for="property">Property:</label>
			<select name="property" id="property">
				<option value=""></option>
				<option value="name">Name</option>
				<option value="open-to">Open-To</option>
				<option value="owner">Owner</option>
				<option value="commander">Commander</option>
				<option value="pilot">Pilot</option>
				<option value="infotext">Infotext</option>
				<option value="crewlist-add">Add to Crewlist</option>
				<option value="crewlist-remove">Remove from Crewlist</option>
				<option value="crewlist-clear">Clear Crewilist</option>
			</select></p>

			<p><label for="value">Value: </label>
			<input type="value" id="value" name="value" value="<?php if(isset($_GET['value'])) {echo $_GET['value'];}; ?>"></p>

			<p><input type="submit" value="submit"></p>
		</form>
		<?php

		if(isset($_GET['faction'], $_GET['entity_type'], $_GET['id'], $_GET['property'], $_GET['value'])) {
			//Do nothing unless all GET fields are set.			
			if($_GET['faction'] != '' && $_GET['entity_type'] != '' && $_GET['id'] != '' && $_GET['property'] != '' && $_GET['value']) {
				//Verify all fields are not empty
				Inventory::submitAction(); //Submit inventory request to API
			} elseif(isset($_GET)) {
				//If all GET fields are not set
				echo "You have not supplied all required parameters.";
			}
		}
		?>
	</div>
<?php
	require 'footer.php';
?>