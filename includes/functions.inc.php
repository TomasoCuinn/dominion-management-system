<?php
include_once 'dbh.inc.php';
include_once 'datecalc.inc.php';
include_once './classes/classTokens.php';

//Format a number to X number of decimal points
	function numForm ($input, $decimal) {
		$output = number_format($input, $decimal);
		return $output;
	}

//Sanatize number, removing commas
	function sanNum ($input) {
		$output = filter_var($input, FILTER_SANITIZE_NUMBER_INT);
		return $output;
	}

//Pull datacard assignments from API and insert into database
	function loadDatacard($db, $dc, $faction, $accessToken) {
		$url = 'https://www.swcombine.com/ws/v2.0/datacard/'.$dc.'/?access_token='.$accessToken;
		$result = simplexml_load_string(file_get_contents($url, false));

		//Build SQL query to insert new data
		$db->connection->begin_transaction();
		$stmt = $db->connection->prepare("INSERT INTO `datacards` (`faction`, `datacard`, `entityID`, `entityType`, `entityName`, `entityOwner`, `assignedQty`) VALUES (?, ?, ?, ?, ?, ?, ?)");

		//Pull datacard name
		$datacardName = $result->datacard->entity;

		//Insert data into datacards table
		foreach($result->datacard->assignedlocations->assignedlocation as $factory) {
			$entityName = ($factory->location);
			$entityID = str_replace(array('4:', '5:'), '', $factory->location['uid'], ); //Remove "4:" & "5:" from ID
			$entityOwner = ($factory->locationowner);
			$entityType = ($factory->locationtype);
			$assignedQty = str_replace("-1", "Unl", $factory->quantity);
			$stmt->bind_param("sssssss", $faction, $datacardName, $entityID, $entityType, $entityName, $entityOwner, $assignedQty);
			$stmt->execute();
		}
		$db->connection->commit();
	}



//Obtain Eng/Ext faction memberlist from API and insert into database
	function updateFactionMembers($db) {
		if(isset($_GET['update'])) {
			if($_GET['update'] == "bseng") {
				//Use Tomas' refresh token to obtain new access/refresh tokens & insert BSEng memberlist into DB
				Tokens::updateAccessToken($db, "Tomas O`Cuinn");
				$accessToken = Tokens::dbAccessToken($db, "Tomas O`Cuinn");
				insertMembers($db, "Blue Star Engineering", $accessToken);
			} elseif($_GET['update'] == "bsext") {
				//Use Pursh's refresh token to obtain new access/refresh tokens & insert BSExt memberlist into DB
				Tokens::updateAccessToken($db, "Pursh Greene");
				$accessToken = Tokens::dbAccessToken($db, "Pursh Greene");
				insertMembers($db, "Blue Star Extractions", $accessToken);
			}  elseif($_GET['update'] == "bsenf") {
				//Use Victor's refresh token to obtain new access/refresh tokens & insert BSEnf memberlist into DB
				Tokens::updateAccessToken($db, "Victor O`Cuinn");
				$accessToken = Tokens::dbAccessToken($db, "Victor O`Cuinn");
				insertMembers($db, "Blue Star Enforcement", $accessToken);
			} else {
				//If _GET update does not == bseng or bsext, return user to index
				header('Location: https://swc-bsd.com/members.php');
			}
		} else {
			//If _GET update is not set, return user to index
			header('Location: https://swc-bsd.com/members.php');
		}
	}

//Used in subsequent updateFactionMembers to insert faction members into database
	function insertMembers($db, $faction, $accessToken) {
		//Use access token to obtain faction memberlist
		$url = 'https://www.swcombine.com/ws/v2.0/faction/'.$faction.'/members/?access_token='.$accessToken;
		$result = simplexml_load_string(file_get_contents($url, false));
	
		//Build SQL query
		$db->connection->begin_transaction();
		$stmt = $db->connection->prepare("INSERT INTO `members` (`handle`, `rank`, `faction`, `joinDate`, `lastLogin`, `active`) VALUES (?, ?, ?, ?, ?, ?) ON DUPLICATE KEY UPDATE `faction` = ?, `lastLogin` = ?, `active` = ?");
		
		//Insert faction members into database
		foreach($result->members->member as $member) {
			$handle = ($member->name);
			$rank = ($member->infofields->infofield);
			$lastLogin = ($member->lastlogin->timestamp);
			$active = ($member->active);
			$joinDate = "0";
			$stmt->bind_param("sssssssss", $handle, $rank, $faction, $joinDate, $lastLogin, $active, $faction, $lastLogin, $active);	
			$stmt->execute();
		}
		$db->connection->commit();
	}
?>