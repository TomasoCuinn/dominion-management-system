<?php
class Database {
	//Properties (similiar to variables)
	private $host;
	private $username;
	private $password;
	private $database;
	public  $connection;

	//Methods (similiar to function)
	function connect() {
		$this->host       = "localhost";
		$this->database   = "members";
		$this->username   = "root";
		$this->password   = "REMOVED";
		$this->connection = new mysqli($this->host, $this->username, $this->password, $this->database);
	}

	function disconnect() {
		mysqli_close($this->connection);
	}
}
?>