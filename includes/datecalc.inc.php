<?php
//Convert SWC timestamp into SWC time (YX, DX, XX:XX:XX)
function formatDate(string $unix): string {
	$swcTime = $unix - 912668400;
	$timeYear = floor((($swcTime) / 86400) / 365);
	$timeDay = ($swcTime / 3600 / 24) % 365 + 1;
	$timeHour = str_pad(($swcTime / 3600) % 24,2,0,STR_PAD_LEFT);
	$timeMin = str_pad(($swcTime / 60) % 60,2,0,STR_PAD_LEFT);
	$timeSec = str_pad(($swcTime) % 60,2,0,STR_PAD_LEFT);
	return "Y".$timeYear." D".$timeDay.", ".$timeHour.":".$timeMin.":".$timeSec;
}

//Convert SWC timestamp into SWC time (YX, DX, XX:XX:XX)
function formatDateYD(string $unix): string {
	$swcTime = $unix - 912668400;
	$timeYear = floor((($swcTime) / 86400) / 365);
	$timeDay = ($swcTime / 3600 / 24) % 365 + 1;
	return "Y".$timeYear." D".$timeDay."";
}

function formatDate2(string $unix): string {
	$swcTime = $unix;
	$timeYear = floor((($swcTime) / 86400) / 365);
	$timeDay = ($swcTime / 3600 / 24) % 365 + 1;
	$timeHour = str_pad(($swcTime / 3600) % 24,2,0,STR_PAD_LEFT);
	$timeMin = str_pad(($swcTime / 60) % 60,2,0,STR_PAD_LEFT);
	$timeSec = str_pad(($swcTime) % 60,2,0,STR_PAD_LEFT);
	return "Y".$timeYear." D".$timeDay.", ".$timeHour.":".$timeMin.":".$timeSec;
}

function swc_time($time, $offset = FALSE){ // TRUE to convert real time() to swc_time. FALSE to convert swc timestamps to swc_time.
	if($offset == TRUE){$time = $time - 912668400;}
	$array = array();
	$array["year"] = floor(($time / 86400) / 365);
	$array["day"] = ($time / 3600 / 24) % 365 + 1;
	$array["hour"] = str_pad(($time / 3600) % 24,2,0,STR_PAD_LEFT);
	$array["minute"] = str_pad(($time / 60) % 60,2,0,STR_PAD_LEFT);
	$array["second"] = str_pad(($time) % 60,2,0,STR_PAD_LEFT);
	$array["timestamp"] = $time;
	$array["date"] = "Year ".$array["year"]." Day ".$array["day"]." ".$array["hour"].":".$array["minute"];
	return $array;
}

function real_time($year, $day, $hour, $minute, $second = 0){
	$years = $year * 31536000;
	$days = $day * 86400;
	$hours = $hour * 3600;
	$minutes = $minute * 60;
	$seconds = $second;
	return $years + $days + $hours + $minutes + $seconds - 86400;
}

/*
$oneYear = "31536000";
$oneMonth = $oneYear / "12";
$oneDay = $oneYear / "365.2425";
$baseY0 = "912668400";
$Year0 = $baseY0;
$Year1 = $baseY0 + $oneYear;
$Year2 = $baseY0 + $oneYear * 2;
$Year3 = $baseY0 + $oneYear * 3;
$Year4 = $baseY0 + $oneYear * 4;
$Year5 = $baseY0 + $oneYear * 5;
$Year6 = $baseY0 + $oneYear * 6;
$Year7 = $baseY0 + $oneYear * 7;
$Year8 = $baseY0 + $oneYear * 8;
$Year9 = $baseY0 + $oneYear * 9;
$Year10 = $baseY0 + $oneYear * 10;
$Year11 = $baseY0 + $oneYear * 11;
$Year12 = $baseY0 + $oneYear * 12;
$Year13 = $baseY0 + $oneYear * 13;
$Year14 = $baseY0 + $oneYear * 14;
$Year15 = $baseY0 + $oneYear * 15;
$Year16 = $baseY0 + $oneYear * 16;
$Year17 = $baseY0 + $oneYear * 17;
$Year18 = $baseY0 + $oneYear * 18;
$Year19 = $baseY0 + $oneYear * 19;
$Year20 = $baseY0 + $oneYear * 20;
$Year21 = $baseY0 + $oneYear * 21;
$Year22 = $baseY0 + $oneYear * 22;
$Year23 = $baseY0 + $oneYear * 23;
$Year24 = $baseY0 + $oneYear * 24;
*/
?>