<?php
	session_start();
	$start_time = microtime(true);
	include_once 'classes/classDatabase.php';
	include_once 'classes/classGNS.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="description" content="Blue Star Dominion GNS Archive">
<meta name="author" content="Tomas o`Cuinn">
<meta name="copyright" content="Blue Star Dominion">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Dominion Management System</title>
<link rel="stylesheet" href="style.css">
<link rel="apple-touch-icon" sizes="180x180" href="/images/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="/images/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="/images/favicon-16x16.png">
<link rel="manifest" href="/site.webmanifest">
</head>
<body>
	<div class="grid-container">
		<div class="item1">
			<h2>Dominion Management System</h2>
		</div>
		<div class="item2">
			<a class="no-list-style" href="login.php">DMS Login</a>
			<a class="no-list-style" href="gns.php">GNS Archive</a>
		</div>

		<div class="item3">
			<h3>GNS Flashnews Archive</h3>
			<p>
			<button onclick="window.location.href='https://swc-bsd.com/gns.php?&records=all';">Show All Records</button></p>
			<p>
			<?php
			$db = new Database(); //Establish connection to database

			GNS::compareEntries($db); //Transfer flashnews from SWC to database
			?>
			<form>
				<label for="search">Search GNS: </label>
				<input type="text" id="search" name="search" minlength="3">
				<input type="submit" value="Submit">
			</form>
			<br />
			<?php
			if(isset($_GET['search'])) {
				GNS::userSearch($db, $_GET['search']); //Perform user search
			} elseif(isset($_GET['records'])) {
				GNS::allRecords($db); //User requests all flashnews records
			} else {
				GNS::defaultRecords($db); //No search defaults to showing 50 latest records
			}
			
			$db->disconnect(); //End connection to database
			?>
			</p>
		</div>

		<div class="item5">
			<!-- Display page load time -->
			<p>Page load time: <?php echo(numForm(microtime(true) - $start_time, 5)); ?> seconds.
			<br />
			<span style="color: gold">Developed by Tomas O`Cuinn</span></p>
		</div>
	</div>
</body>
</html>
