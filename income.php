<?php
	require 'header.php';
	include_once 'classes/classIncome.php';
?>
	<div class="item3">
	<h3>Facility Income Overview</h3>
	<?php
	$income = new Income(); //Perform functions for income class

	echo "<p>The Dominion produced <span class='boldgold'>" . number_format($income->systemIncomeDagu + $income->systemIncomeWoldona) . "</span> credits.</p>";
	echo "<p>Dagu generated <span class='boldgold'>" . number_format($income->systemIncomeDagu) . "</span> credits.</p>";
	echo "<p>Woldona generated <span class='boldgold'>" . number_format($income->systemIncomeWoldona) . "</span> credits.</p>";
	?>
	</div>
	<div class="item3">
		<h3>Income Breakdown by Facility Type</h3>
		<table>
			<tr class="fi">
				<td>Generic</td>
				<td><?php echo number_format($income->incomeGeneric) ?></td>
			</tr>
			<tr class="fi">
				<td>Religious</td>
				<td><?php echo number_format($income->incomeReligious) ?></td>
			</tr>
			<tr class="fi">
				<td>Trading</td>
				<td><?php echo number_format($income->incomeTrading) ?></td>
			</tr>
			<tr class="fi">
				<td>Medical</td>
				<td><?php echo number_format($income->incomeGeneric) ?></td>
			</tr>
		</table>
		<br />
	</div>

	<div class="item3">
		<h3>Income Breakdown by Planet</h3>
		<table>
			<tr class="fi">
				<td>D1</td>
				<td><?php echo number_format($income->planetIncomeD1); ?></td>
			</tr>
			<tr class="fi">
				<td>D2</td>
				<td><?php echo number_format($income->planetIncomeD2); ?></td>
			</tr>
			<tr class="fi">
				<td>D3</td>
				<td><?php echo number_format($income->planetIncomeD3); ?></td>
			</tr>
			<tr class="fi">
				<td>D4</td>
				<td><?php echo number_format($income->planetIncomeD4); ?></td>
			</tr>
			<tr class="fi">
				<td>W1</td>
				<td><?php echo number_format($income->planetIncomeW1); ?></td>
			</tr>
			<tr class="fi">
				<td>W2</td>
				<td><?php echo number_format($income->planetIncomeW2); ?></td>
			</tr>
			<tr class="fi">
				<td>W3</td>
				<td><?php echo number_format($income->planetIncomeW3); ?></td>
			</tr>
			<tr class="fi">
				<td>W4</td>
				<td><?php echo number_format($income->planetIncomeW4); ?></td>
			</tr>
			<tr class="fi">
				<td>W5</td>
				<td><?php echo number_format($income->planetIncomeW5); ?></td>
			</tr>
			<tr class="fi">
				<td>W6</td>
				<td><?php echo number_format($income->planetIncomeW6); ?></td>
			</tr>
			<tr class="fi">
				<td>W7</td>
				<td><?php echo number_format($income->planetIncomeW7); ?></td>
			</tr>
		</table>
		<p><button onclick="window.location.href='https://swc-bsd.com/update/uploadincome.php';">Update FI</button></p>
	</div>

	<div class="item3">
		<h3>Historical Facility Income</h3>
		<table class="members">
			<tr class="fi">
				<td class="bold" id="members">Year</td>
				<td class="bold" id="members"># Weeks</td>
				<td class="bold" id="members">Income</td>
				<td class="bold" id="members">Weekly Average</td>
			</tr>
			<tr class="fi">
				<td id="members">20</td>
				<td id="members"><?php echo $income->countWeeksY20; ?></td>
				<td id="members"><?php echo number_format($income->sumIncomeY20);?></td>
				<td id="members"><?php echo number_format($income->sumIncomeY20 / $income->countWeeksY20); ?></td>
			</tr>
			<tr class="fi">
				<td id="members">21</td>
				<td id="members"><?php echo $income->countWeeksY21; ?></td>
				<td id="members"><?php echo number_format($income->sumIncomeY21);?></td>
				<td id="members"><?php echo number_format($income->sumIncomeY21 / $income->countWeeksY21);?></td>
			</tr>
			<tr class="fi">
				<td id="members">22</td>
				<td id="members"><?php echo $income->countWeeksY22; ?></td>
				<td id="members"><?php echo number_format($income->sumIncomeY22);?></td>
				<td id="members"><?php echo number_format($income->sumIncomeY22 / $income->countWeeksY22);?></td>
			</tr>
			<tr class="fi">
				<td id="members">23</td>
				<td id="members"><?php echo $income->countWeeksY23; ?></td>
				<td id="members"><?php echo number_format($income->sumIncomeY23);?></td>
				<td id="members"><?php echo number_format($income->sumIncomeY23 / $income->countWeeksY23);?></td>
			</tr>
			<tr class="fi">
				<td id="members">24</td>
				<td id="members"><?php echo $income->countWeeksY24; ?></td>
				<td id="members"><?php echo number_format($income->sumIncomeY24);?></td>
				<td id="members"><?php echo number_format($income->sumIncomeY24 / $income->countWeeksY24);?></td>
			</tr>
			<tr class="fi">
				<td id="members">Total</td>
				<td id="members"><?php echo $income->countWeeksAll; ?></td>
				<td id="members"><?php echo number_format($income->sumIncomeAll);?></td>
				<td id="members"><?php echo number_format($income->sumIncomeAll / $income->countWeeksAll);?></td>
			</tr>
		</table>
		<br />
		<form action="income.php" method="post">
			<div>
				<label for="date">Date</label>
				<br />
				<input type="text" name="date" id="date">
			</div>
			<br />
			<div>
				<label for="income">Income</label>
				<br />
				<input type="text" name="income" id="income">
			</div>
			<br />
			<div>
				<label for="tax">Tax Income</label>
				<br />
				<input type="text" name="tax" id="tax">
			</div>
			<br />
			<div>
				<label for="salary">Salary</label>
				<br />
				<input type="text" name="salary" id="salary">
			</div>
			<br />
			<div>
				<button type="submit" value="submit">Submit</button>
			</div>
		</form>
		<?php
		//START of adding new rows to DB
		if(isset($_POST['date'])) {
			$db = new Database(); //Connect to database

			//Insert new record
			$db->connection->begin_transaction();
			$stmt = $db->connection->prepare("INSERT INTO incomeOverTime (`date`, `income`, `tax`, `salary`) VALUES (?, ?, ?, ?)");
			$stmt->bind_param("siii", $date, $income, $tax, $salary);

			$date = ($_POST['date']);
			$income = ($_POST['income']);
			$tax = ($_POST['tax']);
			$salary = ($_POST['salary']);

			$stmt->execute();
			$db->connection->commit();

			$db->disconnect(); //End connection to database
		}
		// END of adding new rows to DB
		?>
	</div>
<?php
	require 'footer.php';
?>
