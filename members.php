<?php
	require 'header.php';
	include_once 'classes/classMembers.php';
?>
	<div class="item3">
		<h3>Dominion Members</h3>
		<p>Update Members
		<br />
		<button onclick="window.location.href='https://swc-bsd.com/members.php?&update=bsenf';">ENF</button>
		<button onclick="window.location.href='https://swc-bsd.com/members.php?&update=bseng';">ENG</button>
		<button onclick="window.location.href='https://swc-bsd.com/members.php?&update=bsext';">EXT</button>
		</p><p>
		<?php
		//Establish connection to database
		$db = new Database();
		$db->connect();

		if (isset($_GET['update'])) {
			Members::updateFactionMembers($db); //If update GET is set, insert memberlist from API into database
		} else {
			Members::displayMembers($db); //Display members if update GET is not set
		}

		//End connection to database
		$db->disconnect();
		unset($db);
		?>
		</p>
	</div>
<?php
	require 'footer.php';
?>
